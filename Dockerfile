FROM docker.io/alpine:3 AS builder
WORKDIR /work
RUN apk add git nodejs yarn
COPY package.json yarn.lock .
RUN yarn install
COPY scripts/ scripts/
COPY src/ src/
COPY tsconfig.json tslint.json webpackfile.js .
RUN NODE_ENV=production ash scripts/compile.sh


FROM docker.io/caddy:2-alpine AS runtime
WORKDIR /srv
COPY --from=builder /work/dist/ .
COPY Caddyfile /etc/caddy/

ENV PORT=3000

CMD ["caddy", "run", "--config", "/etc/caddy/Caddyfile"]
